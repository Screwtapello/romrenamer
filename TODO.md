- Automatically decompress things that look compressed and hash the contents.
- Automatically compress completed games?
- Instead of hashing files as we find them,
  find all the filenames first
  so we know exactly how many we have to process
  so we can draw a progress bar while we're hashing
    - ideally, a progress bar by bytecount,
      rather than just number of files
- Handle the `date` element in the header.
- Maybe don't canonicalize the source directory,
  for more friendly error messages?
- Replace `indicatif` with a spinner that trims text to the terminal width.
