use assert_cmd::prelude::*;
use assert_fs::prelude::*;
use predicates::prelude::*;

#[test]
fn run_twice_does_not_corrupt() {
    let parent = assert_fs::TempDir::new().expect("Could not create temp dir?");

    let datfile_path = parent.child("datfile.xml");
    let input_path = parent.child("inputs");
    let output_path = parent.child("outputs");

    datfile_path
        .write_str(
            r#"
                <?xml version="1.0"?>
                <!DOCTYPE datafile PUBLIC
                    "-//Logiqx//DTD ROM Management Datafile//EN"
                    "http://www.logiqx.com/Dats/datafile.dtd">
                <datafile>
                    <header>
                        <name>datfile name</name>
                    </header>
                    <game name="Some game">
                        <rom name="some-game.rom" size="9"
                            sha1="7b0f6b837c1025e470cac8bd5a86665d6e06b2d0"/>
                    </game>
                </datafile>
            "#,
        )
        .expect("Could not create datfile?");

    // Create our input file.
    input_path
        .child("file-A")
        .write_str("some-game")
        .expect("Could not create some-game.rom");

    // Run the renamer.
    std::process::Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .expect("Could not find cargo-built binary?")
        .arg(datfile_path.path())
        .arg(input_path.path())
        .arg(output_path.path())
        .arg("-vvv")
        .assert()
        .success()
        .stdout(predicates::str::contains("Complete games: 1"));

    // Now the output file should exist, with the expected content...
    output_path
        .child("datfile name/Some game/some-game.rom")
        .assert(
            predicates::str::contains("some-game")
                .from_utf8()
                .from_file_path(),
        );

    // ...and the input file should still exist, unmodified.
    input_path.child("file-A").assert(
        predicates::str::contains("some-game")
            .from_utf8()
            .from_file_path(),
    );

    // Run the renamer a second time.
    std::process::Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .expect("Could not find cargo-built binary?")
        .arg(datfile_path.path())
        .arg(input_path.path())
        .arg(output_path.path())
        .arg("-vvv")
        .assert()
        .success()
        .stdout(predicates::str::contains("Complete games: 1"));

    // The output file should still exist, unmodfied...
    output_path
        .child("datfile name/Some game/some-game.rom")
        .assert(
            predicates::str::contains("some-game")
                .from_utf8()
                .from_file_path(),
        );

    // ...and the input file should still exist, also unmodified.
    input_path.child("file-A").assert(
        predicates::str::contains("some-game")
            .from_utf8()
            .from_file_path(),
    );
}
