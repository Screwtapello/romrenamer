use assert_cmd::prelude::*;
use assert_fs::prelude::*;

#[test]
fn simple_integration() {
    let parent = assert_fs::TempDir::new().expect("Could not create temp dir?");

    let datfile_path = parent.child("datfile.xml");
    let input_path = parent.child("inputs");
    let output_path = parent.child("outputs");

    datfile_path
        .write_str(
            r#"
                <?xml version="1.0"?>
                <!DOCTYPE datafile PUBLIC
                    "-//Logiqx//DTD ROM Management Datafile//EN"
                    "http://www.logiqx.com/Dats/datafile.dtd">
                <datafile>
                    <header>
                        <name>datfile name</name>
                    </header>
                    <game name="Complete game">
                        <rom name="complete.rom" size="8"
                            sha1="0737c22d3bfae812339732d14d8c7dbd6dc4e09c"/>
                        <rom name="bios.rom" size="4"
                            sha1="ac9ff4e0e2a43c3124ee555035d31b42b89a7d6e"/>
                    </game>
                    <game name="Partial game">
                        <rom name="partial.rom" size="7"
                            sha1="355705cf62a56669303d2561f29e0620a676c36e"/>
                        <rom name="bios.rom" size="4"
                            sha1="ac9ff4e0e2a43c3124ee555035d31b42b89a7d6e"/>
                    </game>
                    <game name="Missing game">
                        <rom name="missing.rom" size="7"
                            sha1="5a013c49508291c6816ac388f93a2c11973086ed"/>
                        <rom name="missing-bios.rom" size="12"
                            sha1="830acb508e5dcbf929a8c32fdd0c0e36edf8f812"/>
                    </game>
                </datafile>
            "#,
        )
        .expect("Could not create datfile?");

    input_path
        .child("file-A")
        .write_str("complete")
        .expect("Could not create complete.rom");
    input_path
        .child("file-B")
        .write_str("bios")
        .expect("Could not create bios.rom");
    input_path
        .child("file-C")
        .write_str("unused")
        .expect("Could not create unused.rom");

    std::process::Command::cargo_bin(env!("CARGO_PKG_NAME"))
        .expect("Could not find cargo-built binary?")
        .arg(datfile_path.path())
        .arg(input_path.path())
        .arg(output_path.path())
        .arg("-vvv")
        .assert()
        .success()
        .stderr(predicates::str::contains(
            r#"Game "Partial game" ROM "partial.rom" is missing"#,
        ))
        .stderr(predicates::str::contains(
            r#"Game "Missing game" is completely missing"#,
        ))
        .stderr(predicates::str::contains(format!(
            "Ignoring file not in catalogue: {:?}",
            input_path
                .child("file-C")
                .path()
                .canonicalize()
                .expect("Could not canonicalize valid file?"),
        )));

    output_path
        .child("datfile name/Complete game/complete.rom")
        .assert(predicates::path::eq_file(input_path.child("file-A").path()));
    output_path
        .child("datfile name/Complete game/bios.rom")
        .assert(predicates::path::eq_file(input_path.child("file-B").path()));
    output_path
        .child("datfile name/Partial game/partial.rom")
        .assert(predicates::path::missing());
    output_path
        .child("datfile name/Partial game/bios.rom")
        .assert(predicates::path::missing());
    output_path
        .child("datfile name/Missing game/missing.rom")
        .assert(predicates::path::missing());
    output_path
        .child("datfile name/Missing game/missing-bios.rom")
        .assert(predicates::path::missing());
}
