All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog]
and this project adheres to [Semantic Versioning].

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

[Unreleased]
============

Fixed
-----

  - Quieted some unimportant log messages from "info" to "debug"

[0.1.1] - 2019-04-26
====================

Added
-----

  - romrenamer learned to give a more helpful, complete description
    in its `--help` output.

Fixed
-----

  - romrenamer no longer corrupts the source
    when the destination directory contains files copied in a previous run.
  - romrenamer learned to reject datfiles
    with a missing or empty "name" element in the header.

[0.1.0] - 2019-04-25
====================

Initial release.

[Unreleased]: https://gitlab.com/Screwtapello/romrenamer/compare/v0.1.1...master
[0.1.1]: https://gitlab.com/Screwtapello/romrenamer/tree/v0.1.1
[0.1.0]: https://gitlab.com/Screwtapello/romrenamer/tree/v0.1.0
