romrenamer
==========

A command-line tool to rename game ROM images according to No-Intro DAT files.

Preserving historical video-games can be hard. Games were usually published on
strange media, incompatible with modern PCs. It's possible to build devices
to extract game data and copy it across, but since such devices are often
home-made they're not always reliable. If two people dump different copies of
the same game and get different results, one or both of them might be corrupted,
or perhaps they're legitimately diffferent versions released by the original
publisher, it's hard to say.

Organisations like [No-Intro] catalogue extracted game data, figure out which
dumps are corrupt and which are legitimate releases, and then give each release
a unique, human-readable name to identify it. If you extract a game's data
yourself, it's useful to have a tool that can identify it by consulting the
catalogue.

[No-Intro]: http://no-intro.org/

Building
--------

romrenamer is built in the usual Rust way:

```sh
$ cargo build --release
```

...then look in `target/release/` for the `romrenamer` binary.

There are also precompiled nightly builds available for certain platforms:

  - [Linux x86_64](https://gitlab.com/Screwtapello/romrenamer/-/jobs/artifacts/master/download?job=romrenamer-linux-x86_64-binaries)
  - [Windows x86_64](https://gitlab.com/Screwtapello/romrenamer/-/jobs/artifacts/master/download?job=romrenamer-windows-x86_64-binaries)

Installation
------------

romrenamer doesn't have any runtime dependencies,
so you can just copy the executable to anywhere convenient.

Usage
-----

You will need:

  - One or more classic video game ROMs to be renamed
  - A DAT file that includes the ROM you want to rename;
    suitable DAT files can be obtained from [the No-Intro Dat-o-Matic][dom]
      - "Parent/Clone" DAT files should work,
        but romrenamer doesn't do anything with the parent/clone information

[dom]: http://datomatic.no-intro.org/?page=download

Run the tool like this:

```sh
$ romrenamer DATFILE SOURCEDIR DESTDIR
```

...where:

  - `DATFILE` is the path to the DAT file used for identifying files by hash,
    and their proper filenames
  - `SOURCEDIR` is the path to the directory containing the files to be renamed
      - Make sure the files are uncompressed,
        or they won't match the hashes in the DAT file
      - This directory is searched recursively for files
      - No changes will be made to any files in this directory
  - `DESTDIR` is the path to the directory where files will be stored
    under their new names
      - This path will be created if it does not already exist
      - If this directory is on the same filesystem as `SOURCEDIR`,
        and that filesystem support hard links,
        romrenamer will use them to minimise disk space usage

romrenamer will proceed to find and hash all the files in `SOURCEDIR`,
and look up the hashes in `DATFILE`
to see if they're used by any games.

Once the hashing is complete,
romrenamer will display a summary of what it found:

```
Unused files:   1
Missing games:  1
Partial games:  0
Complete games: 119
Expected games: 120
```

  - "Unused files" are files in `SOURCEDIR`
    whose hashes were not listed in `DATFILE`
  - "Missing games" are games where
    none of the components listed in `DATFILE`
    could be found in `SOURCEDIR`
  - "Partial games" are games where
    some of the components listed in `DATFILE`
    could not be found in `SOURCEDIR`
      - This is only relevant for games that require at least two ROM files
  - "Complete games" are games where
    all of the components listed in `DATFILE`
    were found in `SOURCEDIR`
  - "Expected games" is the total number of games
    listed in `DATFILE`

If you want more information about
which files were unused,
or missing or partial games,
you can use the `-v`/`--verbose` flag when running romrenamer.
Using it once (`-v`)
will show warnings about missing and partial games,
using it twice (`-v -v` or `-vv` for short)
will show information about unused files too.
Using it more times will show more details.

Finally, 
romrenamer will construct a directory structure
for all the complete games
in side `DESTDIR`.

More information
----------------

  - [Official source repository](https://gitlab.com/Screwtapello/romrenamer)
