use std::fs;
use std::io;
use std::path;

use digest::Digest;
use log::warn;

fn files_in_tree<'a, P>(
    root: P,
) -> impl Iterator<Item = ironpath::Absolute> + 'a
where
    P: AsRef<path::Path> + 'a,
{
    ignore::Walk::new(&root).filter_map(move |result| match result {
        Ok(entry) => match entry.file_type() {
            Some(file_type) => {
                // We only care about files.
                if file_type.is_file() {
                    Some(entry.into_path())
                } else {
                    None
                }
            }
            // We don't care about things that don't have a file type.
            _ => None,
        },
        // We don't care about things that raised errors.
        Err(e) => {
            warn!(
                "While walking tree {:?}, ignoring error: {}",
                root.as_ref(),
                e,
            );
            None
        }
    })
    .filter_map(|path| match ironpath::Absolute::new(&path) {
        Ok(p) => Some(p),
        Err(e) => {
            warn!(
                "While canonicalizing {:?}, ignoring error: {}",
                path,
                e,
            );
            None
        }
    })
}

#[derive(Debug, Clone, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub struct HashedFile {
    path: ironpath::Absolute,
    size: u64,
    md5: [u8; 16],
    sha1: [u8; 20],
}

impl HashedFile {
    pub fn with_spinner(
        path: &ironpath::Absolute,
        spinner: &indicatif::ProgressBar,
    ) -> Result<HashedFile, io::Error> {
        let mut handle = fs::File::open(&path)?;
        let mut buffer = vec![0; 8192];
        let mut res = HashedFile {
            path: path.clone(),
            size: 0,
            md5: [0; 16],
            sha1: [0; 20],
        };

        let mut md5_hasher = md5::Md5::new();
        let mut sha1_hasher = sha1::Sha1::new();

        spinner.set_message(&format!("Hashing: {:?}", res.path.as_path()));
        loop {
            use io::Read;
            let chunk_size = handle.read(&mut buffer)?;
            if chunk_size == 0 {
                break;
            }
            res.size += chunk_size as u64;
            let chunk = &buffer[0..chunk_size];
            md5_hasher.input(chunk);
            sha1_hasher.input(chunk);
            spinner.tick();
        }

        res.md5.copy_from_slice(md5_hasher.result().as_slice());
        res.sha1.copy_from_slice(sha1_hasher.result().as_slice());

        Ok(res)
    }

    pub fn path(&self) -> &ironpath::Absolute {
        &self.path
    }
    pub fn size(&self) -> u64 {
        self.size
    }
    pub fn md5(&self) -> &[u8; 16] {
        &self.md5
    }
    pub fn sha1(&self) -> &[u8; 20] {
        &self.sha1
    }
}

pub fn hash_files_in_tree<'a, P>(
    root: P,
    spinner: &'a indicatif::ProgressBar,
) -> impl Iterator<Item = HashedFile> + 'a
where
    P: AsRef<path::Path> + 'a,
{
    files_in_tree(root).filter_map(move |path| {
        match HashedFile::with_spinner(&path, spinner) {
            Ok(hashed_file) => Some(hashed_file),
            Err(e) => {
                warn!("While hashing file {:?}, ignoring error: {}", &path, e);
                None
            }
        }
    })
}
