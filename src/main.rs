use std::error;
use std::fs;
use std::path;

use log::error;
use log::info;
use log::warn;
use structopt::StructOpt;

mod collation;
mod construction;
mod datfile;
mod extraction;

#[derive(StructOpt, Debug)]
#[structopt(about = "Rename files according to the names given in a datfile.")]
struct Config {
    #[structopt(value_name = "DATFILE")]
    /// The path to a No-Intro Standard or Parent/Clone DAT file.
    /// See https://datomatic.no-intro.org/?page=download
    dat_file: path::PathBuf,

    #[structopt(value_name = "SOURCEDIR")]
    /// All files in this directory (recursively) will be hashed
    /// and compared to the hashes listed in DATFILE.
    /// Make sure they're uncompressed!
    source_dir: path::PathBuf,

    #[structopt(value_name = "DESTDIR")]
    /// Complete games that can be built from the files in SOURCEDIR will be
    /// constructed in DESTDIR, using the file and directory names given in
    /// DATFILE.
    dest_dir: path::PathBuf,

    #[structopt(flatten)]
    verbose: clap_verbosity_flag::Verbosity,
}

fn real_main() -> Result<(), Box<dyn error::Error>> {
    let args = Config::from_args();
    args.verbose
        .setup_env_logger("romrenamer")
        .map_err(|e| format!("Could not set up logging: {}", e))?;

    let dat_file = ironpath::Absolute::new(&args.dat_file).map_err(|e| {
        format!("Could not canonicalize path to dat file: {}", e)
    })?;
    let catalogue = {
        let handle = std::fs::File::open(dat_file)
            .map_err(|e| format!("Could not open dat file: {}", e))?;
        datfile::Catalogue::from_file(handle)?
    };

    let source_dir = args
        .source_dir
        .canonicalize()
        .map_err(|e| ironpath::Error::IoError {
            err: e,
            at: args.source_dir.clone(),
        })
        .and_then(ironpath::Absolute::new)
        .map_err(|e| format!("Could not canonicalize source dir: {}", e))?;

    fs::create_dir_all(&args.dest_dir).map_err(|e| {
        format!("Could not create destination directory: {}", e)
    })?;
    let dest_dir = args
        .dest_dir
        .canonicalize()
        .map_err(|e| ironpath::Error::IoError {
            err: e,
            at: args.dest_dir.clone(),
        })
        .and_then(ironpath::Absolute::new)
        .map_err(|e| {
            format!("Could not canonicalize destination dir: {}", e)
        })?;

    if source_dir.as_path().starts_with(&dest_dir) {
        return Err(
            "Source directory must be outside the destination directory."
                .into(),
        );
    }
    if dest_dir.as_path().starts_with(&source_dir) {
        return Err(
            "Source directory must be outside the destination directory."
                .into(),
        );
    }

    let spinner = indicatif::ProgressBar::new_spinner();
    spinner.set_style(
        // behold the dancing worm!
        indicatif::ProgressStyle::default_spinner().tick_chars("->)|(<-<(|)>"),
    );

    let collated_files = collation::FileInfo::new(
        extraction::hash_files_in_tree(source_dir, &spinner),
        &catalogue,
    );

    spinner.finish_and_clear();

    for path in collated_files.unused_files.iter() {
        info!("Ignoring file not in catalogue: {:?}", path.as_path())
    }

    for game_index in collated_files.missing_games.iter() {
        warn!(
            "Game {:?} is completely missing",
            catalogue.games[*game_index].name,
        );
    }

    for game_index in collated_files.partial_games.iter().cloned() {
        let game = &catalogue.games[game_index];

        for (rom_index, rom) in game.roms.iter().enumerate() {
            let romref = datfile::RomRef::new(game_index, rom_index);
            if !collated_files.found_roms.contains_key(&romref) {
                warn!("Game {:?} ROM {:?} is missing", game.name, rom.name);
            }
        }
    }

    println!("Unused files:   {}", collated_files.unused_files.len());
    println!("Missing games:  {}", collated_files.missing_games.len());
    println!("Partial games:  {}", collated_files.partial_games.len());
    println!("Complete games: {}", collated_files.complete_games.len());
    println!("Expected games: {}", catalogue.games.len());

    construction::construct_output(&catalogue, &collated_files, &dest_dir)?;

    Ok(())
}

fn main() {
    match real_main() {
        Ok(_) => (),
        Err(e) => {
            error!("{}", e);
            std::process::exit(1);
        }
    };
}
