use std::collections;

#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct FileInfo {
    pub unused_files: collections::HashSet<ironpath::Absolute>,
    pub found_roms:
        collections::HashMap<crate::datfile::RomRef, ironpath::Absolute>,
    pub complete_games: collections::HashSet<usize>,
    pub partial_games: collections::HashSet<usize>,
    pub missing_games: collections::HashSet<usize>,
}

impl FileInfo {
    pub fn new<I: Iterator<Item = crate::extraction::HashedFile>>(
        hashed_files: I,
        catalogue: &crate::datfile::Catalogue,
    ) -> FileInfo {
        let mut res = FileInfo::default();

        for hashed_file in hashed_files {
            let roms_for_file = catalogue
                .by_sha1
                .get(&(*hashed_file.sha1(), hashed_file.size()))
                .or_else(|| {
                    catalogue
                        .by_md5
                        .get(&(*hashed_file.md5(), hashed_file.size()))
                });

            match roms_for_file {
                Some(roms) => {
                    // Our source file maps to
                    // one or more ROMs in our catalogue!
                    for each in roms.iter() {
                        // If this is the first source we've found for this ROM,
                        // record it, othewise we can safely ignore it.
                        res.found_roms
                            .entry(each.clone())
                            .or_insert_with(|| hashed_file.path().clone());
                    }
                }
                None => {
                    res.unused_files.insert(hashed_file.path().clone());
                }
            }
        }

        for (game_index, game) in catalogue.games.iter().enumerate() {
            let expected_roms = game.roms.len();

            let actual_roms = (0..expected_roms)
                .filter_map(|rom_index| {
                    res.found_roms.get(&crate::datfile::RomRef::new(
                        game_index, rom_index,
                    ))
                })
                .count();

            if actual_roms == expected_roms {
                res.complete_games.insert(game_index);
            } else if actual_roms == 0 {
                res.missing_games.insert(game_index);
            } else {
                res.partial_games.insert(game_index);
            }
        }

        res
    }
}
