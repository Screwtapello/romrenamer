use std::error;
use std::fs;
use std::io;

use log::debug;

use crate::collation;
use crate::datfile;

fn construct_file(
    source: &ironpath::Absolute,
    dest: &ironpath::Absolute,
) -> Result<(), io::Error> {
    // Try to create the parent directory of the given file,
    // if it doesn't already exist.
    let dest_parent = dest.as_path().parent();

    debug!("Creating ROM directory: {:?}", dest_parent);
    dest_parent.map(fs::create_dir_all).transpose()?;

    // In an ideal world, we would hard-link our source-file
    // to a random name in the target directory,
    // then atomically replace the old file (if any) with the new one.
    // Unfortunately, "atomic replace" is not easy to achieve on Windows,
    // so we'll just try to delete the old file first.
    debug!("Removing existing ROM, if any: {:?}", dest.as_path());
    fs::remove_file(&dest).or_else(|e| match e.kind() {
        // Since we're deleting a file, it's fine
        // if the file is already gone.
        io::ErrorKind::NotFound => Ok(()),
        // Other errors are fatal, though.
        _ => Err(e),
    })?;

    debug!(
        "Trying to install {:?} to {:?}",
        source.as_path(),
        dest.as_path()
    );
    fs::hard_link(source, dest).or_else(|e| match e.kind() {
        // We just deleted the destination path, so if it already exists,
        // something super-weird is going on, and we should bail out.
        io::ErrorKind::AlreadyExists => Err(e),
        // Otherwise, this is likely to be a "cannot hard-link across
        // filesystems" error or something like that,
        // so it's reasonable to try copying instead.
        _ => fs::copy(source, dest).map(|_| ()),
    })?;

    Ok(())
}

pub fn construct_output(
    catalogue: &datfile::Catalogue,
    collated_files: &collation::FileInfo,
    dest_dir: &ironpath::Absolute,
) -> Result<(), Box<dyn error::Error>> {
    let progress =
        indicatif::ProgressBar::new(collated_files.complete_games.len() as u64);
    for game_index in
        progress.wrap_iter(collated_files.complete_games.iter().cloned())
    {
        let game = &catalogue.games[game_index];
        for rom_index in 0..game.roms.len() {
            let romref = datfile::RomRef::new(game_index, rom_index);
            let source_file = &collated_files.found_roms[&romref];
            let dest_file =
                dest_dir.join_relative(&catalogue.to_path(&romref)?);

            construct_file(source_file, &dest_file)?;
        }
    }

    progress.finish_and_clear();

    Ok(())
}
