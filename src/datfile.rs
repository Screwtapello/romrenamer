use std::collections;
use std::num;

use log::warn;

use xml::attribute;
use xml::name;
use xml::reader;

#[derive(Debug, Clone, PartialEq)]
pub enum Error {
    XmlError(reader::Error),
    MissingAttribute {
        element: name::OwnedName,
        attribute: name::OwnedName,
    },
    MissingElement {
        parent: name::OwnedName,
        child: name::OwnedName,
    },
    InvalidSize(num::ParseIntError),
    FromHexError(hex::FromHexError),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::XmlError(e) => write!(f, "{}", e),
            Error::MissingAttribute { element, attribute } => write!(
                f,
                "Element {} is missing attribute {}",
                element, attribute,
            ),
            Error::MissingElement { parent, child } => write!(
                f,
                "Element {} is missing child {} (or the child is empty)",
                parent, child
            ),
            Error::InvalidSize(e) => {
                write!(f, "A size attribute contained an invalid value: {}", e)
            }
            Error::FromHexError(e) => {
                write!(f, "A hash could not be hex-decoded: {}", e)
            }
        }
    }
}

impl From<reader::Error> for Error {
    fn from(other: reader::Error) -> Error {
        Error::XmlError(other)
    }
}

impl From<num::ParseIntError> for Error {
    fn from(other: num::ParseIntError) -> Error {
        Error::InvalidSize(other)
    }
}

impl From<hex::FromHexError> for Error {
    fn from(other: hex::FromHexError) -> Error {
        Error::FromHexError(other)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::XmlError(e) => Some(e),
            Error::InvalidSize(e) => Some(e),
            Error::FromHexError(e) => Some(e),
            _ => None,
        }
    }
}

#[derive(Default, Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct RomRef {
    pub game_index: usize,
    pub rom_index: usize,
}

impl RomRef {
    pub fn new(game_index: usize, rom_index: usize) -> RomRef {
        RomRef {
            game_index,
            rom_index,
        }
    }
}

#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct Catalogue {
    pub name: String,
    pub description: String,
    pub version: String,
    pub author: String,
    pub homepage: String,
    pub url: String,
    pub games: Vec<Game>,
    pub by_crc32:
        collections::HashMap<([u8; 4], u64), collections::BTreeSet<RomRef>>,
    pub by_md5:
        collections::HashMap<([u8; 16], u64), collections::BTreeSet<RomRef>>,
    pub by_sha1:
        collections::HashMap<([u8; 20], u64), collections::BTreeSet<RomRef>>,
}

impl Catalogue {
    pub fn from_file<R: std::io::Read>(file: R) -> Result<Catalogue, Error> {
        let parser_config = xml::ParserConfig::new()
            .trim_whitespace(true)
            .cdata_to_characters(true)
            .ignore_comments(true);

        let mut res = Catalogue::default();

        let mut events =
            xml::EventReader::new_with_config(file, parser_config).into_iter();

        // The first thing we see should be the <header/> element,
        // so extract all its metadata.
        let mut current_element: Option<name::OwnedName> = None;
        for event in &mut events {
            match event? {
                reader::XmlEvent::StartElement { name, .. } => {
                    current_element = Some(name);
                }
                reader::XmlEvent::EndElement { name } => {
                    if name.borrow() == "header".into() {
                        break;
                    }
                }
                reader::XmlEvent::Characters(c) => {
                    match current_element
                        .as_ref()
                        .map(|e| e.local_name.as_str())
                    {
                        Some("name") => res.name = c,
                        Some("description") => res.description = c,
                        Some("version") => res.version = c,
                        Some("author") => res.author = c,
                        Some("homepage") => res.homepage = c,
                        Some("url") => res.url = c,
                        _ => warn!("Ignoring unexpected text {:?}", c),
                    }
                }
                _ => (),
            }
        }

        // We use the name field as part of
        // the generated output path, so it needs to be non-empty.
        // It's a required field in the DTD,
        // but it never hurts to be sure.
        if res.name == "" {
            return Err(Error::MissingElement {
                parent: xml::name::OwnedName::local("header"),
                child: xml::name::OwnedName::local("name"),
            });
        }

        // After the header comes all the games and their ROMs.
        let mut current_game = None;
        for event in &mut events {
            match event? {
                reader::XmlEvent::StartElement {
                    name, attributes, ..
                } => match (name.local_name.as_str(), current_game.as_mut()) {
                    ("game", _) => {
                        current_game = Some(Game::from_attributes(attributes)?);
                    }
                    ("rom", Some(game)) => {
                        let rom = Rom::from_attributes(attributes)?;

                        if let Some(ref crc32) = rom.crc32 {
                            res.by_crc32
                                .entry((*crc32, rom.size))
                                .or_default()
                                .insert(RomRef::new(
                                    res.games.len(),
                                    game.roms.len(),
                                ));
                        }

                        if let Some(ref md5) = rom.md5 {
                            res.by_md5
                                .entry((*md5, rom.size))
                                .or_default()
                                .insert(RomRef::new(
                                    res.games.len(),
                                    game.roms.len(),
                                ));
                        }

                        if let Some(ref sha1) = rom.sha1 {
                            res.by_sha1
                                .entry((*sha1, rom.size))
                                .or_default()
                                .insert(RomRef::new(
                                    res.games.len(),
                                    game.roms.len(),
                                ));
                        }

                        game.roms.push(rom);
                    }
                    _ => (),
                },
                reader::XmlEvent::EndElement { name } => {
                    if name.local_name.as_str() == "game" {
                        match current_game.take() {
                            Some(game) => res.games.push(game),
                            None => warn!(
                                "The current game was gone \
                                 before the <game/> element ended?"
                            ),
                        }
                    }
                }
                reader::XmlEvent::Characters(c) => {
                    if let Some(game) = current_game.as_mut() {
                        game.description = c;
                    } else {
                        warn!("Ignoring unexpected characters: {:?}", c);
                    }
                }
                _ => (),
            }
        }

        Ok(res)
    }

    pub fn to_path(
        &self,
        romref: &RomRef,
    ) -> Result<ironpath::Relative, ironpath::Error> {
        let game = &self.games[romref.game_index];
        Ok(ironpath::Relative::new(&self.name)?
            .join(&game.name)?
            .join(&game.roms[romref.rom_index].name)?)
    }
}

#[derive(Default, Debug, Clone, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub struct Game {
    pub name: String,
    pub description: String,
    pub roms: Vec<Rom>,
}

impl Game {
    fn from_attributes(
        attributes: Vec<attribute::OwnedAttribute>,
    ) -> Result<Game, Error> {
        let mut res = Game::default();

        for each in attributes {
            if each.name.local_name == "name" {
                res.name = each.value
            }
        }

        if res.name == "" {
            return Err(Error::MissingAttribute {
                element: name::OwnedName::local("game"),
                attribute: name::OwnedName::local("name"),
            });
        }
        Ok(res)
    }
}

#[derive(Default, Debug, Clone, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub struct Rom {
    pub name: String,
    pub size: u64,
    pub status: Option<String>,
    pub crc32: Option<[u8; 4]>,
    pub md5: Option<[u8; 16]>,
    pub sha1: Option<[u8; 20]>,
}

impl Rom {
    fn from_attributes(
        attributes: Vec<attribute::OwnedAttribute>,
    ) -> Result<Rom, Error> {
        let mut res = Rom {
            ..Default::default()
        };

        let mut name: Option<String> = None;
        let mut size: Option<u64> = None;
        let mut crc32: Option<String> = None;
        let mut md5: Option<String> = None;
        let mut sha1: Option<String> = None;

        for each in attributes {
            match each.name.local_name.as_str() {
                "name" => name = Some(each.value),
                "size" => size = Some(each.value.parse()?),
                "crc" => crc32 = Some(each.value),
                "md5" => md5 = Some(each.value),
                "sha1" => sha1 = Some(each.value),
                "status" => res.status = Some(each.value),
                local_name => {
                    warn!("Ignoring unrecognised attribute {}", local_name)
                }
            }
        }

        res.name = name.ok_or(Error::MissingAttribute {
            element: name::OwnedName::local("rom"),
            attribute: name::OwnedName::local("name"),
        })?;

        res.size = size.ok_or(Error::MissingAttribute {
            element: name::OwnedName::local("rom"),
            attribute: name::OwnedName::local("size"),
        })?;

        res.crc32 = crc32.map(hex::FromHex::from_hex).transpose()?;
        res.md5 = md5.map(hex::FromHex::from_hex).transpose()?;
        res.sha1 = sha1.map(hex::FromHex::from_hex).transpose()?;

        Ok(res)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    static DATFILE: &'static str = r#"
        <?xml version="1.0"?>
        <!DOCTYPE datafile PUBLIC
            "-//Logiqx//DTD ROM Management Datafile//EN"
            "http://www.logiqx.com/Dats/datafile.dtd">
        <datafile>
            <header>
                <name>datfile name</name>
                <description>datfile description</description>
                <version>v1.2.3</version>
                <author>datfile author</author>
                <homepage>website name</homepage>
                <url>http://example.com/</url>
            </header>
            <game name="Some Game">
                <description>Some game's description</description>
                <rom name="prg.rom" size="1024"
                    crc="11111111"
                    md5="11111111111111111111111111111111"
                    sha1="1111111111111111111111111111111111111111"/>
                <rom name="chr.rom" size="2048"
                    crc="22222222"
                    md5="22222222222222222222222222222222"
                    sha1="2222222222222222222222222222222222222222"/>
            </game>
            <game name="Another Game">
                <description>Another game's description</description>
                <rom name="prg.rom" size="4096"
                    crc="33333333"
                    md5="33333333333333333333333333333333"
                    sha1="3333333333333333333333333333333333333333"/>
                <rom name="chr.rom" size="8192"
                    crc="44444444"
                    md5="44444444444444444444444444444444"
                    sha1="4444444444444444444444444444444444444444"/>
            </game>
        </datafile>
    "#;

    #[test]
    fn read_metadata() {
        let catalogue = Catalogue::from_file(DATFILE.as_bytes())
            .expect("Could not parse valid datfile?");

        assert_eq!(catalogue.name, "datfile name");
        assert_eq!(catalogue.description, "datfile description");
        assert_eq!(catalogue.version, "v1.2.3");
        assert_eq!(catalogue.author, "datfile author");
        assert_eq!(catalogue.homepage, "website name");
        assert_eq!(catalogue.url, "http://example.com/");
        assert_eq!(catalogue.games.len(), 2);

        assert_eq!(catalogue.games[0].name, "Some Game");
        assert_eq!(catalogue.games[0].description, "Some game's description");
        assert_eq!(catalogue.games[0].roms.len(), 2);
        assert_eq!(catalogue.games[0].roms[0].name, "prg.rom");
        assert_eq!(catalogue.games[0].roms[0].size, 1024);
        assert_eq!(catalogue.games[0].roms[1].name, "chr.rom");
        assert_eq!(catalogue.games[0].roms[1].size, 2048);

        assert_eq!(catalogue.games[1].name, "Another Game");
        assert_eq!(
            catalogue.games[1].description,
            "Another game's description"
        );
        assert_eq!(catalogue.games[1].roms.len(), 2);
        assert_eq!(catalogue.games[1].roms[0].name, "prg.rom");
        assert_eq!(catalogue.games[1].roms[0].size, 4096);
        assert_eq!(catalogue.games[1].roms[1].name, "chr.rom");
        assert_eq!(catalogue.games[1].roms[1].size, 8192);
    }

    #[test]
    fn index_by_crc32() {
        let catalogue = Catalogue::from_file(DATFILE.as_bytes())
            .expect("Could not parse valid datfile?");

        assert!(catalogue.by_crc32[&(*b"\x11\x11\x11\x11", 1024)]
            .contains(&RomRef::new(0, 0)));
        assert!(catalogue.by_crc32[&(*b"\x22\x22\x22\x22", 2048)]
            .contains(&RomRef::new(0, 1)));
        assert!(catalogue.by_crc32[&(*b"\x33\x33\x33\x33", 4096)]
            .contains(&RomRef::new(1, 0)));
        assert!(catalogue.by_crc32[&(*b"\x44\x44\x44\x44", 8192)]
            .contains(&RomRef::new(1, 1)));

        assert_eq!(catalogue.by_crc32.get(&(*b"\x11\x22\x33\x44", 1)), None)
    }

    #[test]
    fn index_by_md5() {
        let catalogue = Catalogue::from_file(DATFILE.as_bytes())
            .expect("Could not parse valid datfile?");

        assert!(catalogue.by_md5[&(
            *b"\x11\x11\x11\x11\x11\x11\x11\x11\
                    \x11\x11\x11\x11\x11\x11\x11\x11",
            1024
        )]
            .contains(&RomRef::new(0, 0)));
        assert!(catalogue.by_md5[&(
            *b"\x22\x22\x22\x22\x22\x22\x22\x22\
                    \x22\x22\x22\x22\x22\x22\x22\x22",
            2048
        )]
            .contains(&RomRef::new(0, 1)));
        assert!(catalogue.by_md5[&(
            *b"\x33\x33\x33\x33\x33\x33\x33\x33\
                    \x33\x33\x33\x33\x33\x33\x33\x33",
            4096
        )]
            .contains(&RomRef::new(1, 0)));
        assert!(catalogue.by_md5[&(
            *b"\x44\x44\x44\x44\x44\x44\x44\x44\
                    \x44\x44\x44\x44\x44\x44\x44\x44",
            8192
        )]
            .contains(&RomRef::new(1, 1)));

        assert_eq!(
            catalogue.by_md5.get(&(
                *b"\x11\x11\x11\x11\x22\x22\x22\x22\
                    \x33\x33\x33\x33\x44\x44\x44\x44",
                1,
            )),
            None,
        )
    }

    #[test]
    fn index_by_sha1() {
        let catalogue = Catalogue::from_file(DATFILE.as_bytes())
            .expect("Could not parse valid datfile?");

        assert!(catalogue.by_sha1[&(
            *b"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\
                    \x11\x11\x11\x11\x11\x11\x11\x11\x11\x11",
            1024
        )]
            .contains(&RomRef::new(0, 0)));
        assert!(catalogue.by_sha1[&(
            *b"\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\
                    \x22\x22\x22\x22\x22\x22\x22\x22\x22\x22",
            2048
        )]
            .contains(&RomRef::new(0, 1)));
        assert!(catalogue.by_sha1[&(
            *b"\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\
                    \x33\x33\x33\x33\x33\x33\x33\x33\x33\x33",
            4096
        )]
            .contains(&RomRef::new(1, 0)));
        assert!(catalogue.by_sha1[&(
            *b"\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\
                    \x44\x44\x44\x44\x44\x44\x44\x44\x44\x44",
            8192
        )]
            .contains(&RomRef::new(1, 1)));

        assert_eq!(
            catalogue.by_sha1.get(&(
                *b"\x11\x11\x11\x11\x11\x22\x22\x22\x22\x22\
                    \x33\x33\x33\x33\x33\x44\x44\x44\x44\x44",
                1,
            )),
            None,
        )
    }

    #[test]
    fn read_broken_xml() {
        let err = Catalogue::from_file(
            &br#"
            <datafile>
                <header>
                    <name></gnome>
            "#[..],
        )
        .expect_err("Parsed broken xml?");

        match err {
            Error::XmlError(e) => match e.kind() {
                xml::reader::ErrorKind::Syntax(msg) => assert_eq!(
                    msg,
                    "Unexpected closing tag: gnome, expected name"
                ),
                _ => {
                    eprintln!("Unexpected error: {}", e);
                    assert!(false);
                }
            },
            _ => {
                eprintln!("Unexpected error: {}", err);
                assert!(false);
            }
        }
    }

    #[test]
    fn reject_empty_datfile_name() {
        let err = Catalogue::from_file(
            &br#"
            <datafile>
                <header>
                    <name></name>
                </header>
            </datafile>
            "#[..],
        )
        .expect_err("Parsed empty name?");

        match err {
            Error::MissingElement { parent, child } => {
                assert_eq!(parent.namespace, None);
                assert_eq!(parent.local_name, "header");
                assert_eq!(child.namespace, None);
                assert_eq!(child.local_name, "name");
            }
            _ => {
                eprintln!("Unexpected error: {}", err);
                panic!();
            }
        }
    }

    #[test]
    fn reject_missing_datfile_name() {
        let err = Catalogue::from_file(
            &br#"
            <datafile>
                <header>
                </header>
            </datafile>
            "#[..],
        )
        .expect_err("Parsed empty name?");

        match err {
            Error::MissingElement { parent, child } => {
                assert_eq!(parent.namespace, None);
                assert_eq!(parent.local_name, "header");
                assert_eq!(child.namespace, None);
                assert_eq!(child.local_name, "name");
            }
            _ => {
                eprintln!("Unexpected error: {}", err);
                panic!();
            }
        }
    }
}
